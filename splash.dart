
//
import 'package:page_transition/page_transition.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_module_q3/screens/main_login.dart';
//import 'main_login.dart';
// ignore: camel_case_types
class splashScreen extends StatefulWidget {
  const splashScreen({ Key? key }) : super(key: key);

  @override
  State<splashScreen> createState() => _splashScreenState();
}
// ignore: camel_case_types
class _splashScreenState extends State<splashScreen> {
 
  @override
  Widget build(BuildContext context) {
    return  AnimatedSplashScreen(
    splash: Column(
      children: [
        Image.asset('assets/image1.jpg'),
         const Text('Best Maid Africa',style: TextStyle(fontSize: 40,fontWeight: FontWeight.bold,color: Colors.green),),
      ],
    ),   
    backgroundColor:  Colors.white,
     nextScreen: const mainPage(), 
     splashIconSize: 250,
     duration: 400,
     splashTransition: SplashTransition.rotationTransition,
     pageTransitionType: PageTransitionType.rightToLeftWithFade,
    );
  }
}