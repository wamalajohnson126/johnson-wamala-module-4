// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'main_login.dart';
// ignore: camel_case_types
class thirdPage extends StatefulWidget {
  const thirdPage({ Key? key }) : super(key: key);
  @override
  State<thirdPage> createState() => _thirdPageState();
}
// ignore: camel_case_types
class _thirdPageState extends State<thirdPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       // ignore: unnecessary_new
       theme: new ThemeData(
         primaryColor: Colors.green,
         brightness: Brightness.dark
       ),
        home: Scaffold(
     appBar: AppBar(
       title: const Text("Registration Page"),
     ),
     body: Center(
       child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          children: <Widget> [
            Column(
              // ignore: prefer_const_literals_to_create_immutables
              children:<Widget> [
                const SizedBox(height: 120,),
                const SizedBox(height: 30,),
                const Text("Register Here!" ,style: TextStyle(fontSize: 25,color: Colors.blue),)
              ],
            ),
             const SizedBox(height: 60.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "First Name:",
                labelStyle: TextStyle(fontSize: 20,color: Colors.blue),
                filled: true,
               )
            ),
             const SizedBox(height: 20.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "Surname Name:",
                labelStyle: TextStyle(fontSize: 20,color: Colors.blue),
                filled: true,
               )
            ),
             const SizedBox(height: 20.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "Email:",
                labelStyle: TextStyle(fontSize: 20,color: Colors.blue),
                filled: true,
               )
            ),
            const SizedBox(height: 20.0,),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                labelText: " Enter Password:",
                labelStyle: TextStyle(fontSize: 20.0,color: Colors.blue),
                filled: true,
              ),
            ),
            const SizedBox(height: 20.0,),
            const TextField(
               obscureText: true,
              decoration: InputDecoration(
                labelText: "Re-enter Password:",
                labelStyle: TextStyle(fontSize: 20,color: Colors.blue),
                filled: true,
               )
            ),
             const SizedBox(height: 20.0,),
          Column(
            children: <Widget> [
              ButtonTheme(height: 80.0,
               disabledColor: Colors.greenAccent,
               child: InkWell(
            child: const Text("Save and Login",style: TextStyle(fontSize: 25.0,color: Colors.green),),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => const mainPage() ));
            },
          )
               ),              
            ],
          ),
          ],
        ), 
     ),
        ),
    );
  }
}