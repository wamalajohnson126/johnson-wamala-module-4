import 'package:flutter/material.dart';
import 'package:flutter_module_q3/screens/dashboard.dart';
import 'package:flutter_module_q3/screens/edit_your_profile.dart';
import 'package:flutter_module_q3/screens/feature1.dart';
import 'package:flutter_module_q3/screens/feature2.dart';
import 'package:flutter_module_q3/screens/main_login.dart';
import 'package:flutter_module_q3/screens/register.dart';
import 'screens/splash.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
    theme: ThemeData(
    //primaryColor: Colors.purple,
    //hintColor: Colors.deepOrange,

    ),
    initialRoute: "/splash",
    routes: {
      "/":(context)  => const mainPage(),
      "/dashbord":(context) => const secondPage(),
      "/register":(context) => const thirdPage(),
      "/feature":(context) => const forthPage(),
      "/feature2":(context) => const fifthPage(),
      "/edit":(context) => const sixthPage(),
      "/splash":(context) => const splashScreen(),
       
    },
    );
  }
}

