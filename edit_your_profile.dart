import 'package:flutter/material.dart';
import 'dashboard.dart';
// ignore: camel_case_types
class sixthPage extends StatefulWidget {
  const sixthPage({ Key? key }) : super(key: key);
  @override
  State<sixthPage> createState() => _sixthPageState();
}
// ignore: camel_case_types
class _sixthPageState extends State<sixthPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       // ignore: unnecessary_new
       theme: new ThemeData(
         primaryColor: Colors.green[300],
         brightness: Brightness.dark
       ),
        home: Scaffold(
     appBar: AppBar(
       title: const Text("Edit Profile Page"),
     ),
     body: Center(
       child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          children: <Widget> [
            Column(
              // ignore: prefer_const_literals_to_create_immutables
              children:<Widget> [
                const SizedBox(height: 120,),
                const SizedBox(height: 30,),
                const Text("Edit Profile Here!" ,style: TextStyle(fontSize: 25,color: Colors.blue),)
              ],
            ),
             const SizedBox(height: 60.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "First Name:",
                labelStyle: TextStyle(fontSize: 20,color: Colors.blue),
                filled: true,
               )
            ),
             const SizedBox(height: 20.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "Surname Name:",
                labelStyle: TextStyle(fontSize: 20,color: Colors.blue),
                filled: true,
               )
            ),
             const SizedBox(height: 20.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "Email:",
                labelStyle: TextStyle(fontSize: 20,color: Colors.blue),
                filled: true,
               )
            ),
            const SizedBox(height: 20.0,),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                labelText: " Phone Number:",
                labelStyle: TextStyle(fontSize: 20.0,color: Colors.blue),
                filled: true,
              ),
            ),
            const SizedBox(height: 20.0,),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                labelText: "Password",
                labelStyle: TextStyle(fontSize: 20.0,color: Colors.blue),
                filled: true,
              ),
            ),
             const SizedBox(height: 20.0,),
          Column(
            children: <Widget> [
              ButtonTheme(height: 80.0,
               disabledColor: Colors.greenAccent,
                child: InkWell(
            child: const Text("Save",style: TextStyle(fontSize: 25.0,color: Colors.green),),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => const secondPage() ));
            },
          )
               ),
               const SizedBox(height: 20.0,),
         InkWell(
            child: const Text("Go back to Dashboard",style: TextStyle(fontSize: 15.0,color: Colors.blue),),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => const secondPage() ));
            },
          )
            ],
          ), 
          ],
        ),         
     ),
        ),
    );
  }
}

